const express = require('express');
const app = express();

// LLAMO A LAS VARIABLES DE ENTORNO

const env = require('./variables.entorno.json')
const node_env = process.env.NODE_ENV || "development";
const port_env = env[node_env];

let telefonos = [{
    marca: "Samsung",
    modelo: "S10",
    gama: "Alta",
    pantalla: "19:9",
    sistema_operativo: "Android",
    precio: "1000"
},{
    marca: "Iphone",
    modelo: "12 Pro",
    gama: "Alta",
    pantalla: "OLED",
    sistema_operativo: "iOS",
    precio: "1200"
},{
    marca: "Nokia",
    modelo: "1100",
    gama: "Baja",
    pantalla: "LCD",
    sistema_operativo: "Series 40",
    precio: "150"
},{
    marca: "Xiaomi",
    modelo: "Mi 9T Pro",
    gama: "Media",
    pantalla: "LCD",
    sistema_operativo: "Android 9 Pie",
    precio: "450"
}]

// CREO ENDPOINT PARA LISTAR TELÉFONOS
app.get('/telefonos',(req,res)=>{
    res.json(telefonos);
});

// CREO ENDPOINT PARA LISTAR EL TELÉFONO MÁS ECONÓMICO
app.get('/tel_barato',(req,res)=>{
    let tel_barato = parseInt(telefonos[0].precio);
    let menor = 0;
    telefonos.forEach((telefono,i) => {
        let tel_precio = parseInt(telefono.precio);
        if (tel_precio<tel_barato){tel_barato = tel_precio; menor = i;}
    });
    res.json(telefonos[menor]);
});

//CREO ENDPOINT PARA TELÉFONO MÁS CARO
app.get('/tel_caro',(req,res)=>{
    let tel_caro = parseInt(telefonos[0].precio);
    let mayor = 0;
    telefonos.forEach((telefono,i) => {
        let tel_precio = parseInt(telefono.precio);
        if(tel_precio>tel_caro){tel_caro = tel_precio; mayor = i;}
    });
    res.json(telefonos[mayor]);
});

//CREO ENDPOINT PARA CATEGORÍAS
app.get('/categorias',(req,res)=>{
    let baja = [];
    let media = [];
    let alta = [];

    telefonos.forEach(telefono => {
        (telefono.gama == "Baja") ? baja.push(telefono) : (telefono.gama == "Media") ? media.push(telefono) : alta.push(telefono);
    });
    
    res.send({alta,media,baja});
});

// CONFIGURACIÓN Alta LISTEN DEL SERVIDOR
app.listen(port_env.port, function(){
    console.log('Se,media,altarvidor corriendo normalmente por el puerto:',port_env.port)
});