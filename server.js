const express = require('express');
const app = express();

//Llamamos al puerto por medio del json
const env = require("./variables.entorno.json");
const node_env = process.env.NODE_ENV || "development";
const port_env = env[node_env];

console.log(port_env.port);

//Configurar los Middleware
//Este método se llama como middleware
app.use(express.json());
//es un método incorporado en express para reconocer el objeto de solicitud entrante como objeto JSON.
//Este método se llama como middleware
app.use(express.urlencoded({ extended: true }));

//Agregar endpoint de saludos

app.get("/", function(req,res){
    console.log("Hola mundo!");
    res.json("Hola Mundo");
});

app.get("/saludo", function(req,res){
    res.json("Hola sin query params");
});

app.get("/saludo", function(req,res){
    res.json("Hola bienvenido " + req.params.nombre);
});
//Agregar endpoint de usuarios

let telefonos = [{
    marca: "Samsung",
    modelo: "S10",
    gama: "Alta",
    pantalla: "19:9",
    sistema_operativo: "Android",
    precio: "1000"
},{
    marca: "Iphone",
    modelo: "12 Pro",
    gama: "Alta",
    pantalla: "OLED",
    sistema_operativo: "iOS",
    precio: "1200"
},{
    marca: "Nokia",
    modelo: "1100",
    gama: "Baja",
    pantalla: "LCD",
    sistema_operativo: "Series 40",
    precio: "150"
},{
    marca: "Xiaomi",
    modelo: "Mi 9T Pro",
    gama: "Media",
    pantalla: "LCD",
    sistema_operativo: "Android 9 Pie",
    precio: "450"
}]

//
app.get('/', function (req, res) {
    res.send('Hola mundo');
  });

app.get('/telefonos', function(req,res){
    res.json(telefonos);
});

app.get('/tel_medio', function(req,res){
    res.json(telefonos.splice(0,(telefonos.length/2)));
});
  
// app.get('/tel_barato', (req,res)=>{
//     let barato = 0;
//     telefonos.forEach((telefono,i) => {
//         if(barato<telefono.precio){
//             barato = i;
//         }
//     });
//     console.log(i)
//     res.json(telefonos[i]);
// });

app.get('/tel_barato', (req,res)=>{
    let barato = 0;
    for (let i = 0; i < telefonos.length; i++) {
        if(barato < telefono.precio){
          barato = i;
        }
        res.json(telefonos[i]);
    }
});
  
//Configuración del listen del servidor

app.listen(3000, function(){
console.log("Servidor corriendo sobre el puerto: 3000");
});